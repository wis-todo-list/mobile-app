// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
/* eslint-disable no-unused-vars */
/* stylesheets */
import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'
/* javascript and vue components */
import Vue from 'vue'
import VueOnsen from 'vue-onsenui'
import store from './store'
import App from './App'

Vue.config.productionTip = false

Vue.use(VueOnsen)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  template: '<App/>',
  components: { App }
})
